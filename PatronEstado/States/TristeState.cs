﻿using PatronEstado.Abstract;
using PatronEstado.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronEstado.States
{
    public class TristeState : State
    {
        public override void Jugar(MyContext context)
        {
            Console.WriteLine("De verdad no tengo ganas de jugar");
        }
        public override void Hambriento(MyContext context)
        {
            Console.WriteLine("Me pongo de mal humor cuando no como algo, gracias!");
            context.State = new FelizState();
        }

        public override string ComoEstas()
        {
            return "Re sad ando";
        }
    }
}
