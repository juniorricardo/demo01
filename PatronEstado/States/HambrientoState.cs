﻿using PatronEstado.Abstract;
using PatronEstado.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronEstado.States
{
    public class HambrientoState : State
    {
        public override void Jugar(MyContext context)
        {
            Console.WriteLine("No tengo ganas de jugar, porque quiero comer algo antes");
        }
        public override void Hambriento(MyContext context)
        {
            Console.WriteLine("Si! vamos a comer algo");
            context.State = new FelizState();
        }
        public override string ComoEstas()
        {
            return "Con hambre xd";
        }
    }
}
