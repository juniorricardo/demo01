﻿using PatronEstado.Abstract;
using PatronEstado.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronEstado.States
{
    public class EnojadoState : State
    {
        public override void Jugar(MyContext context)
        {
            Console.WriteLine("No quiero jugar!! no me molestes!"); ;
        }
        public override void Aburrido(MyContext context)
        {
            base.Aburrido(context);
            Console.WriteLine("Y ademas enojado!");
        }
        public override void Hambriento(MyContext context)
        {
            Console.WriteLine("Si! vamos a comer!");
            context.State = new FelizState();
        }

        public override string ComoEstas()
        {
            return "No pregunte!!";
        }
    }
}
