﻿using PatronEstado.Abstract;
using PatronEstado.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronEstado.States
{
    public class FelizState : State
    {


        public override string ComoEstas()
        {
            return "Estoy feliz!, gracias!";
        }
    }
}
