﻿using PatronEstado.Abstract;
using PatronEstado.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronEstado.States
{
    public class AburridoState : State
    {
        public override void Aburrido(MyContext context)
        {
            base.Aburrido(context);
            context.State = new TristeState();
        }
        public override void Hambriento(MyContext context)
        {
            base.Hambriento(context);
            Console.WriteLine("De verdad no quiero comer, me da fiaca");
        }

        public override string ComoEstas()
        {
            return "Aburriiido, que es esto?!";
        }
    }
}
