﻿using PatronEstado.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PatronEstado
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            for (int i = 0; i < 100; i++)
            {
                Console.Clear();
                Console.WriteLine(rand.Next(35000000,47000000));
                Console.ReadKey();
            }

            #region Menu tamagotchi

            MyContext miTamagotchi = new MyContext();

            while (true)
            {
                Thread.Sleep(2000);
                miTamagotchi.State.ComoEstas();

                Console.WriteLine("menu\n1)Jugar\n2)Dar de comer" +
                                  "\n3) Molestar\n4)Alegrar");
                Console.WriteLine(">Seleccione una opcion.");
                switch (Console.ReadLine())
                {
                    case "1":
                        miTamagotchi.Jugar();
                        break;
                    case "2":
                        miTamagotchi.Alimentar();
                        break;
                    case "3":
                        miTamagotchi.Enojar();
                        break;
                    case "4":
                        miTamagotchi.Alegrar();
                        break;
                    default:
                        break;
                }
            } 
            #endregion

        }
    }
}
