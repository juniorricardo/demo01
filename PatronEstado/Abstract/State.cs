﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatronEstado.Context;
using PatronEstado.States;

namespace PatronEstado.Abstract
{
    public abstract class State
    {
        public virtual void Jugar(MyContext context)
        {
            Console.WriteLine("Vamos a jugar!");
            context.State = new FelizState();
        }
        public virtual void Aburrido(MyContext context)
        {
            Console.WriteLine("Estoy muy aburrido!");
            context.State = new AburridoState();
        }
        public virtual void Feliz(MyContext context)
        {
            Console.WriteLine("Estoy muy feliz!");
            context.State = new FelizState();
        }
        public virtual void Hambriento(MyContext context)
        {
            Console.WriteLine("Tengo hambre");
            context.State = new AburridoState();
        }
        public virtual void Triste(MyContext contexto)
        {
            Console.WriteLine("Estoy sad");
            contexto.State = new TristeState();
        }
        public virtual void Enojar(MyContext contexto)
        {
            Console.WriteLine("Porque me molestas?");
            contexto.State = new EnojadoState();
        }
        public virtual string ComoEstas()
        {
            return "Estoy muy State!";
        }
    }
}
