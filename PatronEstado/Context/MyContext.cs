﻿using PatronEstado.Abstract;
using PatronEstado.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronEstado.Context
{
    public class MyContext
    {
        State state;

        public State State { get => state; set => state = value; }

        public MyContext()
        {
            this.State = new TristeState();
        }

        public void Jugar()
        {
            this.State.Jugar(this);
        }
        public void Aburrir()
        {
            this.State.Aburrido(this);
        }
        public void Alegrar()
        {
            this.State.Feliz(this);
        }
        public void Alimentar()
        {
            this.State.Hambriento(this);
        }
        public void Triste()
        {
            this.State.Triste(this);
        }
        internal void Enojar()
        {
            this.State.Enojar(this);
        }
        public void ComoEstasTama()
        {
            this.State.ComoEstas();
        }

    }
}
