﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.Model
{
    [Table("Equipo_Usuario")]
    public class Equipo_Usuario
    {
        public int Id { get; set; }
        public int EquipoId { get; set; }
        public int UsuarioId { get; set; }
        public Usuario Usuario{ get; set; }
        public Equipo Equipo{ get; set; }
        public bool Disponible { get; set; }
    }
}
