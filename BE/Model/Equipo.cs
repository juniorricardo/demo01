﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.Model
{
    /* Eager Loading -> cargar relaciones explicitas
     * Include con Lambda
     * var personaInclude = db.Persona.Include(x => x.Direcc).FirstOrDefault();
     * var primerDireccionInclude = personaInclude.Direcciones[0];
     * 
     * Include segundo nivel
     * var personasConDireccionesConSub = db.Persona.Include(x => x.Direcciones.Select(y => y.SubDireccion)).FirstOrDefault();
     * var subCalle = personasConDireccionesConSub.Direcciones[0].SubDireccion[0].SubCalle;
     * 
     * Lazy Loading -> podremos acceder a las propiedades relacionada en el momento que 
     *                 decidamos acceder a ellas, es decir, se realizara una consulta en ese momento
     *                 por lo que puede ser mas lento.
     */
    [Table("Equipo")]
    public class Equipo
    {
        public int Id { get; set; }
        public Usuario Lider { get; set; }
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        public virtual ICollection<Usuario> Integrantes { get; set; }

        public Equipo()
        {
            this.Integrantes = new HashSet<Usuario>();
        }
    }
}
