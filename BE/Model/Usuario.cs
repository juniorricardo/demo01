﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE.Model
{

    [Table("Usuario")]
    public class Usuario
    {
        public int Id { get; set; }
        [StringLength(20)]
        public string Nombre { get; set; }
        [StringLength(23)]
        public string Apellido { get; set; }
        public int Dni { get; set; }
        [Display(Name = "Cumpleaños")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Cumple { get; set; }
        [StringLength(100)]
        public string Correo { get; set; }
        public string Resumen { get { return $"{Nombre} {Apellido}"; } }

        public virtual ICollection<Equipo> Equipos { get; set; }

        public Usuario()
        {
            this.Equipos = new HashSet<Equipo>();
        }
    }
}
