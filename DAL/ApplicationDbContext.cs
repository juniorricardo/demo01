﻿using BE.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    // Configurar caracteristicas de nuestra DB
    // Querys
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("EF.CodeFirst.ManyToMany") { }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Equipo> Equipo { get; set; }

        //public DbSet<Equipo_Usuario> Equipo_Usuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().HasKey(x => x.Id);
            modelBuilder.Entity<Equipo>().HasKey(x => x.Id);

            // Ignorar propiedad Resumen de la clase Usuario, para que 
            // no sea mapeado
            modelBuilder.Entity<Usuario>().Ignore(x => x.Resumen);

            // many to many 
            //modelBuilder.Entity<Usuario>().HasMany(x => x.Equipos).WithMany(x => x.Integrantes);

            //version 2
            modelBuilder.Entity<Equipo>().HasMany(x => x.Integrantes)
                                         .WithMany(x => x.Equipos)
                                         .Map(m =>
                                         {
                                             m.ToTable("Equipo_Usuario");
                                             m.MapLeftKey("EquipoId");
                                             m.MapRightKey("UsuarioId");
                                         });

            //modelBuilder.Entity<Equipo_Usuario>().HasKey(x => new { x.EquipoId, x.UsuarioId });

            base.OnModelCreating(modelBuilder);
        }
    }
}
